(function(){

	angular
		.module("starter")
		.controller("HomeCtrl", HomeCtrl);

	HomeCtrl.$inject = ["$cordovaFacebook", "$cordovaSocialSharing", "$cordovaCamera", "$cordovaImagePicker","LoginModal","$ionicPopup","$ionicLoading","$scope", "$state", "$ionicActionSheet", "$http"];

	function HomeCtrl($cordovaFacebook, $cordovaSocialSharing, $cordovaCamera, $cordovaImagePicker, LoginModal, $ionicPopup, $ionicLoading, $scope, $state, $ionicActionSheet, $http){
		var vm = this;

		vm.loginViaFB = function(){
			$cordovaFacebook
    		.login(["public_profile", "email", "user_friends"])
    		.then(function(success){
    			//access token retrieved from FB 

    			//get publiv profile information
    			$cordovaFacebook
    				.api("me", ["public_profile"])
    				.then(function(success){
    					//sample response: {"name":"Fsf Singapore","id":"211293779315682"}
    					alert(success.name);
    				}, function(err){
    					//do something if err
    				});

    		}, function(error){
    			//do something if err
    		});
		}



		vm.shareThisItem = function(){
			$cordovaSocialSharing
				//message, subject, file, link
				.share(null, null, null, "http://google.com")
				.then(function(result){
					//share(message, subject, file, link)
				}, function(err){
					//do something
				});
		};

		vm.imageTaken = ""
		vm.selectMedia = function(){
			var options = {
				maximumImagesCount: 1,
				width: 800,
				height:800,
				quality: 80
			}

			$cordovaImagePicker
				.getPictures(options)
				.then(function(results){
					if(results.length > 0){
						vm.imageTaken = results[0];
					}
				}, function(error){
					//handle the error
				});
		}


		vm.takePicture = function(){
			var options = {
				//DATA_URL / FILE_URI
				destinationType: Camera.DestinationType.FILE_URI,
				sourceType: Camera.PictureSourceType.CAMERA
			};

			$cordovaCamera
				.getPicture(options)
				.then(function(imageUri){
					vm.imageTaken = imageUri;
				}, function(err){
					//error do something
				});
		}


		vm.showLogin = function(){
			LoginModal
				.initialized()
				.then(function(mdl){
					mdl.show();
				});
		}

		vm.myPopup = function(){
			var confirmPopup = $ionicPopup.confirm({
				title: 'Watch out!',
				template: 'Exit this app?'
			});

			confirmPopup.then(function(res) {
				if(res) {
					console.log('Ok');
				} else {
					console.log('Cancel');
				}
			});

		}

		vm.submitToServer = function(){
			$ionicLoading.show({
		      template: '<ion-spinner icon="android"></ion-spinner>'
		    });

		    //$ionicLoading.hide();
		}

		vm.imgFeed = [{"id":10,"img":"http://mxcdn02.mundotkm.com/2017/06/241504donpng-1.png"}];
		vm.updateList = function(){

			$http({	
				method: "GET",
				url: "http://demo5964607.mockable.io/insta",
				data: { },
				headers: {
					'Content-type': 'application/json'
				}
			}).then(function successCallBack(response){
				//check response
				if(response.status == 200){
					var img = response.data.images;
					for(var i = 0; i < response.data.images.length ; i++){
		              vm.imgFeed.push({"id":img[i].id,"img":img[i].img});
		            }   
				}

				$scope.$broadcast('scroll.refreshComplete');
			}, function errorCallBack(){

				$scope.$broadcast('scroll.refreshComplete');
			});
		}

		vm.shareThisImage = function(){
			$ionicActionSheet.show({
				buttons: [
					{ text: 'Facebook' },
					{ text: 'Twitter' }
				],
				titleText: 'Shares',
				cancelText: 'Close',
				destructiveText: 'Delete',
				cancel: function() {
					// add cancel code..
				},
				buttonClicked: function(index) {
					//check index for certain action:
					//if (index == 0) {}
					console.log(index);
					return true;
				},
				destructiveButtonClicked: function(){
					console.log("destructive");
					return true;
				}
			});
		}

		vm.stillHaveData = true;
		vm.photos = [{"id":10,"img":"http://mxcdn02.mundotkm.com/2017/06/241504donpng-1.png"}];
		vm.loadMore = function(){
			$http({	
				method: "GET",
				url: "http://demo5964607.mockable.io/insta",
				data: { },
				headers: {
					'Content-type': 'application/json'
				}
			}).then(function successCallBack(response){
				//check response
				if(response.status == 200){
					var img = response.data.images;
					for(var i = 0; i < response.data.images.length ; i++){
		              vm.photos.push({"id":img[i].id,"img":img[i].img});
		            }

		            if(vm.photos.length > 30){
		            	vm.stillHaveData = false;
		            }

		            //if response.data.images.length == 0
		            //vm.stillHaveData = false;
		            
		            $scope.$broadcast('scroll.infiniteScrollComplete');
				}
			}, function errorCallBack(){});
		}
	}

})();