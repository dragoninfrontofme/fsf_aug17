(function(){

	angular
		.module('starter')
		.service('LoginModal', LoginModal);

	LoginModal.$inject = ["$ionicModal", "$rootScope"];

	function LoginModal($ionicModal, $rootScope){

		var vm = this;

		vm.initialized = function($scope){
			var self = $scope || $rootScope.$new();

			var promise = $ionicModal.
				fromTemplateUrl('templates/login-modal.html', {
					scope: self,
					animation: 'slide-in-up'
				}).then(function(modal){
					self.modal = modal;
					return modal;
				});

			self.closeMe = function(){
				self.modal.hide();
			}

			self.login = function(){
				//do something
			}

			return promise;
		}

	}

})();