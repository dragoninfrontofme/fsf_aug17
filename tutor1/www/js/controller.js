(function(){

	angular
		.module("starter")
		.controller("LoginCtrl", LoginCtrl)
		.controller("HomeCtrl", HomeCtrl);

	LoginCtrl.$inject = ["$state", "$http", "$ionicHistory"];

	function LoginCtrl($state, $http, $ionicHistory){
		var vm = this;

		vm.user = {
			email: '',
			password: ''
		}

		vm.errorMsg = '';

		vm.doLogin = function(){
			if(vm.user.email == ""){
				vm.errorMsg = "Email can't be empty";				
			}else if(vm.user.password == ""){
				vm.errorMsg = "Password can't be empty";				
			}else{

				$http({	
					method: "POST",
					url: "http://demo5964607.mockable.io/login",
					data: { "email": vm.user.email, "password": vm.user.password },
					headers: {
						'Content-type': 'application/json'
					}
				}).then(function successCallBack(response){
					//check response 
					$state.go("tab.home");
				}, function errorCallBack(){
					//do something i.e logging
				});

			}
		}
	}	

	HomeCtrl.$inject = ["$state", "$http", "$ionicHistory"];

	function HomeCtrl($state, $http, $ionicHistory){
		var vm = this;

		vm.imgFeed = [];

		//get data from server:
		$http({	
			method: "GET",
			url: "http://demo5964607.mockable.io/insta",
			data: { },
			headers: {
				'Content-type': 'application/json'
			}
		}).then(function successCallBack(response){
			//check response
			if(response.status == 200){
				var img = response.data.images;
				for(var i = 0; i < response.data.images.length ; i++){
	              vm.imgFeed.push({"id":img[i].id,"img":img[i].img});
	            }

	            console.log(vm.imgFeed);
			}
		}, function errorCallBack(){
			//do something i.e logging
		});

		vm.doLogout = function(){
			$state.go("login");
		}
	}

})();