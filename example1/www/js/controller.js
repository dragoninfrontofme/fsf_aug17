(function(){

	angular
		.module("starter")
		.controller("LoginCtrl", LoginCtrl)
		.controller("HomeCtrl", HomeCtrl);

	LoginCtrl.$inject = ["$state"];

	function LoginCtrl($state){
		var vm = this;

		vm.login = function(){
			/*
				http post:{
					
					//if ok
					$state.go("home");
					//if not ok

				}
			*/
			$state.go("home");
		};
	}

	HomeCtrl.$inject = ["$state"];

	function HomeCtrl($state){
		var vm = this;

		vm.logout = function(){
			$state.go("login");
		};
	}


})();
